# Sensible Number Sort
Sorting text is easy enough, until the text has numbers in it. This output, for example, probably isn't what you would want.
```
Item 1
Item 10
Item 3
```

Sensible Number Sort gets you the output you might expect.
```
Item 1
Item 3
Item 10
```

## How does it work?
Strings to be sorted are each split into a list of elements, alternating numbers and strings. Then we can compare pairs of elements appropriately.

Implementations are provided in a few languages.
