import re
from functools import cmp_to_key
from locale import strcoll
import argparse
from sys import stdin

splitter = re.compile(
    '''(?x) # comments and whitespace
    (?<![\\d+-.]) # start of
    (?=[+-]?\\d)  #          number
    |             # ...or...
    (?<=\\d.)     # end of
    (?!\\d.)      #        number
    '''
)

def getFloat( s ):
    try:
        return float ( s )
    except:
        return None

def compLimit( n ):
    if n < 0:
        return -1
    if n > 0:
        return 1
    return 0

def comparator( s1, s2 ):
    a1 = re.split( splitter, s1.strip() )
    a2 = re.split( splitter, s2.strip() )
    limit = min( len( a1 ), len( a2 ) )
    i = 0

    while i < limit:
        n1 = getFloat( a1 [ i ] )
        n2 = getFloat( a2 [ i ] )
        if n1 and n2:
            numCompare = compLimit( n1 - n2 );
            if 0 != numCompare:
                result = numCompare
                return result
        strCompare = strcoll( a1 [ i ], a2 [ i ] )
        if 0 != strCompare:
            return strCompare
        i += 1
    lenCompare = compLimit( len( a1 ) - len( a2 ) )
    return lenCompare

def sort ( lines, rev ):
    return sorted(
        lines,
        key = cmp_to_key( comparator ),
        reverse = rev
    )

def getLines( fileName ):
    if fileName:
        with open(fileName) as f:
            return [ line for line in f ]
    else:
        return stdin.readlines()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'Sort strings containing numbers sensibly.'
    )
    parser.add_argument(
        'fileName',
        nargs = '?',
        help = 'A file name - if not given, sort stdin'
    )
    parser.add_argument(
        '-r', '--reverse',
        action = 'store_true',
        help = 'Reverse the sort order'
    )
    args = parser.parse_args()

    for line in sort( getLines ( args.fileName ), args.reverse ):
        print( line.strip() )
