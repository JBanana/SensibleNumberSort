import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.Scanner;

public class SensibleNumberSort {
	private static final Pattern SPLITTER = Pattern.compile (
		"""
		(?x)          # comments and whitespace
		(?<![\\d+-.]) # start of
		(?=[+-]?\\d)  #          number
		|             # ...or...
		(?<=[\\d.])   # end of
		(?![\\d.])    #        number
		"""
	);

	public static void main ( final String [] args )
			throws IOException {
		boolean reverse = false;
		String fileName = null;

		switch ( args.length ) {
			case 0:
				// nothing to do
				break;
			case 1:
				if ( "-r".equals ( args [ 0 ] ) || "/r".equals ( args [ 0 ] ) )
					reverse = true;
				else
					fileName = args [ 0 ];
				break;
			case 2:
				if ( "-r".equals ( args [ 0 ] ) || "/r".equals ( args [ 0 ] ) )
					reverse = true;
				else
					throw new IOException ( "Unrecognised parameter: " + args [ 0 ] );
				fileName = args [ 1 ];
				break;
			default:
				throw new IOException ( "Wrong number of arguments - " + args.length + " - expected 0 to 2." );
		}

		final List < String > lines = getLines ( fileName );

		sort ( lines, reverse );

		lines.forEach ( System.out::println );
	}

	private static void sort ( final List < String > lines, final boolean reverse ) {
		lines.sort (
			( s1, s2 ) -> {
				final String [] a1 = SPLITTER.split ( s1.trim() );
				final String [] a2 = SPLITTER.split ( s2.trim() );

				for ( int i = 0, min = Math.min ( a1.length, a2.length ); i < min; i++ ) {
					final Double n1 = getDouble ( a1 [ i ] );
					final Double n2 = getDouble ( a2 [ i ] );
					if ( null != n1 && null != n2 ) {
						final int numCompare = n1.compareTo ( n2 );
						if ( 0 != numCompare ) return reverse ? -numCompare : numCompare;
					}

					final int strCompare = a1 [ i ].compareTo ( a2 [ i ] );
					if ( 0 != strCompare ) return reverse ? -strCompare : strCompare;
				}
				int lenCompare = Integer.valueOf ( a1.length ).compareTo ( a2.length );
				return reverse ? -lenCompare : lenCompare;
			}
		);
	}

	private static List < String > getLines ( final String fileName )
			throws IOException {
		if ( null == fileName )
			try ( final Scanner scanner = new Scanner ( System.in ) ) {
				final List < String > result = new ArrayList <> ();

				while ( scanner.hasNextLine () )
					result.add ( scanner.nextLine () );

				return result;
			}
		else
			return Files.readAllLines ( new File ( fileName ).toPath () );
	}

	private static Double getDouble ( final String s ) {
		try { return Double.valueOf ( s ); }
		catch ( final NumberFormatException x ) { return null; }
	}
}
